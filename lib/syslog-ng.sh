#!/bin/bash

###########################################################
# syslog-ng
###########################################################

function syslog_ng_install {
  log "Installing syslog-ng"

  aptitude install -y syslog-ng
}

function setup_loggly {
  cp /etc/syslog-ng/syslog-ng.conf /etc/syslog-ng/syslog-ng.conf.default

  if [ ! -n "$1" ];
      then LOGGLY_PORT="46306"
      else LOGGLY_PORT="$1"
  fi

  cat <<EOT >/etc/syslog-ng/syslog-ng.conf
@version: 3.3
@include "scl.conf"

# Syslog-ng configuration file, compatible with default Debian syslogd
# installation.

# First, set some global options.
options { chain_hostnames(off); flush_lines(0); use_dns(no); use_fqdn(no);
  owner("root"); group("adm"); perm(0640); stats_freq(0);
  bad_hostname("^gconfd$");
};

###
# Loggly
###
source s_all {
  internal();
  unix-stream("/dev/log");
};

destination d_loggly {
  tcp("logs.loggly.com" port($LOGGLY_PORT));
};

log {
  source(s_all); destination(d_loggly);
};


###
# Include all config files in /etc/syslog-ng/conf.d/
###
@include "/etc/syslog-ng/conf.d/"
EOT
}

