#!/bin/bash

###########################################################
# ruby-build - https://github.com/sstephenson/ruby-build
###########################################################

function ruby_build_install {
  _install_ruby_prerequisites

  git clone git://github.com/sstephenson/ruby-build.git /tmp/ruby-build
  cd /tmp/ruby-build
  ./install.sh
  cd
}

function rbenv_install {
  RBENV_ROOT="/usr/local/rbenv"
  git clone git://github.com/sstephenson/rbenv.git $RBENV_ROOT

  # Add rbenv to the path
  echo "# rbenv setup" > /etc/profile.d/rbenv.sh
  echo "export RBENV_ROOT=$RBENV_ROOT" >> /etc/profile.d/rbenv.sh
  echo 'export PATH="$RBENV_ROOT/bin:$PATH"' >> /etc/profile.d/rbenv.sh
  echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh
  chmod +x /etc/profile.d/rbenv.sh
  source /etc/profile.d/rbenv.sh
  export PATH=$PATH:/usr/local/bin  # make current session see ruby-build
}

function ruby_install {
  if [ ! -n "$1" ];
      then RUBY_VERSION="2.0.0-p247"
      else RUBY_VERSION="$1"
  fi
  rbenv install $RUBY_VERSION
  rbenv global  $RUBY_VERSION
  rbenv rehash

  _disable_ri_rdoc
}

function bundler_install {
  gem install bundler
  rbenv rehash
}


# Private functions ===========================================================

function _disable_ri_rdoc {
  echo "install: --no-ri --no-rdoc" >  /etc/gemrc
  echo "update:  --no-ri --no-rdoc" >> /etc/gemrc
  cp /etc/gemrc /root/.gemrc
  cp /etc/gemrc /etc/skel/.gemrc
}

function _install_ruby_prerequisites {
  aptitude install -y build-essential \
                      zlib1g \
                      zlib1g-dev \
                      libssl-dev \
                      libreadline-dev \
                      gcc \
                      curl \
                      libxml2-dev \
                      libxslt-dev \
                      imagemagick
}
