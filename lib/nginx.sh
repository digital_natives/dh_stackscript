#!/bin/bash

###########################################################
# nginx
###########################################################

function nginx_install {
  log "Installing nginx"

  add-apt-repository -y ppa:nginx/stable
  apt-get update
  apt-get upgrade --show-upgraded

  aptitude install -y nginx
}
