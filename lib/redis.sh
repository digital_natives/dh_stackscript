#!/bin/bash

###########################################################
# redis
###########################################################

function redis_install {
  log "Installing redis"
  aptitude install -y python-software-properties
  add-apt-repository -y ppa:rwky/redis
  apt-get update

  # Redis 2.6+
  aptitude install -y redis-server

  cp /etc/redis/redis.conf /etc/redis/redis.conf.default

  sed -e -i 's/^daemonize no/daemonize yes/'               /etc/redis/redis.conf
  sed -e -i 's/^logfile stdout/logfile /var/log/redis.log' /etc/redis/redis.conf
}

