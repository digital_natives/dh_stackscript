#!/bin/bash

###########################################################
# memcached
###########################################################

function memcached_install {
  log "Installing memcached..."
  aptitude install -y memcached
}
