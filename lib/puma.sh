#!/bin/bash

###########################################################
# puma webserver - jungle toolkit
###########################################################

function puma_jungle_toolkit_install {
  log 'Installing puma - jungle toolkit'
  curl -o /etc/init.d/puma \
       https://raw.github.com/puma/puma/master/tools/jungle/init.d/puma
  curl -o /usr/local/bin/run-puma \
       https://raw.github.com/puma/puma/master/tools/jungle/init.d/run-puma

  chmod +x /etc/init.d/puma
  chmod +x /usr/local/bin/run-puma

  touch /etc/puma.conf
}
