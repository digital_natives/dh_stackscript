function log {
  echo "`date '+%D %T'` $1 "
}

function send_finished_mail {
  RDNS=$(get_rdns_primary_ip)

  cat > ~/setup_message <<EOD
  Hi,

  Your Linode VPS configuration is completed.

EOD


  if [ "$SETUP_MONIT" == "Yes" ]; then
      cat >> ~/setup_message <<EOD
  Monit web interface is at http://${RDNS}:2812/ (use your system username/password).

EOD
  fi

  cat >> ~/setup_message <<EOD
  To access your server ssh to $USER_NAME@$RDNS

  Best,
  Gergo, Digital Natives

  ---

EOD

  cat /root/stackscript.log >> ~/setup_message

  mail -s "Your Linode VPS is ready" "$NOTIFY_EMAIL" < ~/setup_message
}
