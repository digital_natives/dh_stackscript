#!/bin/bash

ROOT_PATH=$(dirname $(readlink -f $0))
LIB_PATH="$ROOT_PATH/lib"

source "$LIB_PATH/1-linode-stackscript.sh"
source "$LIB_PATH/utils.sh"
source "$LIB_PATH/system.sh"
source "$LIB_PATH/system_ubuntu.sh"


log 'Start system update ======================================================'
system_update
system_install_mercurial
system_start_etc_dir_versioning #start recording changes of /etc config files


log 'Configuring hostname ====================================================='
system_update_hostname "$SYS_HOSTNAME"
system_record_etc_dir_changes "Updated hostname"


log 'Create user account ======================================================'
USER_GROUPS=sudo
system_add_user "$USER_NAME" "$USER_PASSWORD" "$USER_GROUPS" "$USER_SHELL"
if [ "$USER_SSHKEY" ]; then
    system_user_add_ssh_key "$USER_NAME" "$USER_SSHKEY"
fi
system_record_etc_dir_changes "Added unprivileged user account"


log 'Configure sshd ==========================================================='
system_sshd_permitrootlogin "$SSHD_PERMITROOTLOGIN"
system_sshd_passwordauthentication "$SSHD_PASSWORDAUTH"
touch /tmp/restart-ssh
system_record_etc_dir_changes "Configured sshd"

# Lock user account if not used for login
if [ "SSHD_PERMITROOTLOGIN" == "No" ]; then
    system_lock_user "root"
    system_record_etc_dir_changes "Locked root account"
fi


log 'Install Postfix =========================================================='
postfix_install_loopback_only
system_record_etc_dir_changes "Installed postfix loopback"


log 'Setup logcheck ==========================================================='
system_security_logcheck
system_record_etc_dir_changes "Installed logcheck"


log 'Setup fail2ban ==========================================================='
system_security_fail2ban
system_record_etc_dir_changes "Installed fail2ban" # SS124


log 'Setup firewall ==========================================================='
system_security_ufw_configure_basic
system_record_etc_dir_changes "Configured UFW" # SS124

if [ -n "$SYS_PRIVATE_IP" ]; then
  system_configure_private_network "$SYS_PRIVATE_IP"
  system_record_etc_dir_changes "Configured private network"
fi


log 'Install system utils ====================================================='
system_install_utils
system_record_etc_dir_changes "Installed common utils"


log 'Setup syslog-ng =========================================================='
SETUP_SYSLOG_NG="Yes"
if [ "$SETUP_SYSLOG_NG" == "Yes" ]; then
  source "$LIB_PATH/syslog-ng.sh"
  syslog_ng_install
  setup_loggly "46306" # Port
  system_record_etc_dir_changes "Installed syslog-ng"
fi


log 'Install PostgreSQL and setup database =========================================================='
if [ "$SETUP_POSTGRESQL" == "Yes" ]; then
  source "$LIB_PATH/postgres.sh" # lib-postgresql
  postgresql_install
  system_record_etc_dir_changes "Installed PostgreSQL"
  postgresql_create_user "$POSTGRESQL_USER" "$POSTGRESQL_PASSWORD"
  postgresql_create_database "$POSTGRESQL_DATABASE" "$POSTGRESQL_USER"
  system_record_etc_dir_changes "Configured PostgreSQL"
fi


log 'Install MongoDB =========================================================='
if [ "$SETUP_MONGODB" == "Yes" ]; then
  source "$LIB_PATH/mongodb.sh"
  mongodb_install
  system_record_etc_dir_changes "Installed MongoDB"
fi


log 'Install Redis ============================================================'
if [ "$SETUP_REDIS" == "Yes" ]; then
  source "$LIB_PATH/redis.sh"
  redis_install
  system_record_etc_dir_changes "Installed Redis"
fi


log 'Install Memcached ========================================================'
if [ "$SETUP_MEMCACHED" == "Yes" ]; then
  source "$LIB_PATH/memcached.sh"
  memcached_install
  system_record_etc_dir_changes "Installed Memcached"
fi


log 'Install nginx ============================================================'
if [ "$SETUP_NGINX" == "Yes" ]; then
  source "$LIB_PATH/nginx.sh"
  nginx_install
  system_record_etc_dir_changes "Installed nginx"
fi


log 'Install nodejs ==========================================================='
# if [ "$SETUP_NODEJS" == "Yes" ]; then
  source "$LIB_PATH/nodejs.sh"
  nodejs_install
  system_record_etc_dir_changes "Installed nodejs"
# fi


log 'Install ruby ============================================================='
source "$LIB_PATH/rbenv.sh"
ruby_build_install
rbenv_install
ruby_install $RUBY_VERSION
bundler_install

source "$LIB_PATH/puma.sh"
puma_jungle_toolkit_install


log 'Restart services ========================================================='
restart_services
restart_initd_services

log 'Setup Monit =============================================================='
if [ "$SETUP_MONIT" == "Yes" ]; then
  source "$LIB_PATH/monit.sh"
  monit_install
  system_record_etc_dir_changes "Installed Monit"

  monit_configure_email "$NOTIFY_EMAIL"
  monit_configure_web 127.0.0.1 # $(system_primary_ip)
  log 'You should setup nginx proxy for monit'
  system_record_etc_dir_changes "Configured Monit interfaces"

  monit_def_system "$SYS_HOSTNAME"
  monit_def_rootfs
  monit_def_cron
  monit_def_postfix
  monit_def_ping_google
  if [ "$SETUP_NGINX" == "Yes" ];      then monit_def_nginx; fi
  if [ "$SETUP_POSTGRESQL" == "Yes" ]; then monit_def_postgresql; fi
  if [ "$SETUP_MONGODB" == "Yes" ];    then monit_def_mongodb; fi
  if [ "$SETUP_MEMCACHED" == "Yes" ];  then monit_def_memcached; fi
  if [ "$SETUP_REDIS" == "Yes" ];      then monit_def_redis; fi
  if [ "$SETUP_SYSLOG_NG" == "Yes" ];  then monit_def_syslog_ng; fi
  system_record_etc_dir_changes "Created Monit rules for installed services"
  monit reload
fi

log 'Send info message ========================================================'
send_finished_mail
