#!/bin/bash
#
# Installs a complete web environment with nginx, rbenv, MongoDB and Redis.
#

# <UDF name="notify_email" Label="Send email notification to" example="Email address to send notification and system alerts. Check Spam folder if you don't receive a notification within 6 minutes." />

# <UDF name="user_name" label="Unprivileged user account name" example="This is the account that you will be using to log in." />
# <UDF name="user_password" label="Unprivileged user password" />
# <UDF name="user_sshkey" label="Public Key for user" default="" example="Recommended method of authentication. It is more secure than password log in." />
# <UDF name="sshd_passwordauth" label="Use SSH password authentication" oneof="Yes,No" default="No" example="Turn off password authentication if you have added a Public Key." />
# <UDF name="sshd_permitrootlogin" label="Permit SSH root login" oneof="No,Yes" default="No" example="Root account should not be exposed." />

# <UDF name="user_shell" label="Shell" oneof="/bin/zsh,/bin/bash" default="/bin/bash" />

# <UDF name="sys_hostname" label="System hostname" default="myvps" example="Name of your server, i.e. linode1. http://en.wikipedia.org/wiki/Category:James_Bond_characters" />
# <UDF name="sys_private_ip" Label="Private IP" default="" example="Configure network card to listen on this Private IP (if enabled in Linode/Remote Access settings tab). See http://library.linode.com/networking/configuring-static-ip-interfaces" />

# <UDF name="ruby_version"    label="Ruby version" default="2.0.0-p247" example="https://github.com/sstephenson/ruby-build/tree/master/share/ruby-build" />

# <UDF name="setup_nginx"     label="Install nginx"                    oneof="Yes,No" default="Yes" />
# <UDF name="setup_mongodb"   label="Install MongoDB"                  oneof="Yes,No" default="Yes" />
# <UDF name="setup_redis"     label="Install Redis"                    oneof="Yes,No" default="Yes" />
# <UDF name="setup_memcached" label="Install Memcached"                oneof="Yes,No" default="No" />
# <UDF name="setup_rbenv"     label="Install rbenv"                    oneof="Yes,No" default="Yes" />
# <UDF name="setup_monit"     label="Install Monit system monitoring?" oneof="Yes,No" default="Yes" />

# <UDF name="setup_postgresql" label="Configure PostgreSQL and create database?" oneof="Yes,No" default="Yes" />
# <UDF name="postgresql_database" label="PostgreSQL database name" example="PostgreSQL database name, ASCII only." default="" />
# <UDF name="postgresql_user" label="PostgreSQL database user" example="PostgreSQL database user name, ASCII only." default="" />
# <UDF name="postgresql_password" label="PostgreSQL user password" default="" />


set -e
set -u
#set -x

exec &> /root/stackscript.log

echo "Installing git and cloning the shellstack script..."
apt-get install -y git-core

git clone https://bitbucket.org/digital_natives/dh_stackscript.git
dh_stackscript/install.sh

